<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="<c:url value='/resources/assets/css/fontawesome/font-awesome.min.css'/>"  >
</head>
<body>
	<aside class="left-panel ">

		<a href="homePage.do">
			<div class="user text-center">
				<img src="resources/assets/images/metro.png" class="img-circle">
			</div>
		</a>

		<nav class="navigation">
			<ul class="list-unstyled">
				<li id="dashbordli"><a href="homePage.do" title="Home"><i
						class="fa fa-home"></i><span class="nav-label">Home</span></a></li>
				<li id="dashbordli"><a href="users.do" title="User"><i
						class="fa fa-user"></i><span class="nav-label">User</span></a></li>
				<li id="dashbordli"><a href="station.do" title="Station"><i
						class="fa fa-subway"></i><span class="nav-label">Station</span></a></li>
				<li id="dashbordli"><a href="slot.do" title="Slots"><i
						class="fa fa-subway"></i><span class="nav-label">Slots</span></a></li>
			</ul>
		</nav>
	</aside>
	<!-- Aside Ends-->
</body>
</html>