<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="banner-text agileinfo about-bnrtext"> 
				<div class="container">
					<h2><a href="index.html">Home</a> / Gallery</h2> 
				</div>
			</div>
			<!-- //banner-text -->   
		</div>
	</div>
	<!-- //banner --> 
	<!-- gallery -->
	<div class="gallery">  
		<div class="container">  
			<div class="agileits-title">
				<h3>Our Gallery</h3>
			</div>   
			<div class="gallery-agileinfo">
				<ul class="w3gallery-grids">
					<li>
						<a href="<c:url value='/resources/client/images/k1.jpg'/>">
							<img src="<c:url value='/resources/client/images/k1.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>KOCHI METRO</h4> 
							</div> 
						</a>
					</li> 
					<li>
						<a href="<c:url value='/resources/client/images/i3.jpg'/>">
							<img src="<c:url value='/resources/client/images/i3.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>KOCHI METRO</h4> 
							</div> 
						</a>
					</li> 
					<li>
						<a href="<c:url value='/resources/client/images/i1.jpg'/>">
							<img src="<c:url value='/resources/client/images/i1.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>KOCHI METRO</h4> 
							</div> 
						</a>
					</li> 
					<li>
						<a href="<c:url value='/resources/client/images/i5.jpg'/>">
							<img src="<c:url value='/resources/client/images/i5.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>KOCHI METRO</h4> 
							</div> 
						</a>
					</li>  
					<br>
					<br>
					<br>
					<li><div class="container_12"><a href="km.html"><br>
<br><br><br><br><center>					KOCHI METRO FOR MORE DETAILS</center></a>
						<a href="<c:url value='/resources/client/images/i6.jpg'/>">
							<img src="<c:url value='/resources/client/images/i6.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>Our Gallery</h4> 
							</div> 
						</a>
					</li>
					<li>
						<a href="<c:url value='/resources/client/images/slide3.jpg'/>">
							<img src="<c:url value='/resources/client/images/slide3.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>KOCHI METRO</h4> 
							</div> 
						</a>
					</li> 
					<li>
						<a href="<c:url value='/resources/client/images/k2.jpg'/>">
							<img src="<c:url value='/resources/client/images/k2.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>KOCHI METRO</h4> 
							</div> 
						</a>
					</li> 	
					<li>
						<a href="<c:url value='/resources/client/images/i2.jpg'/>">
							<img src="<c:url value='/resources/client/images/i2.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>KOCHI METRO</h4> 
							</div> 
						</a>
					</li> 
					<li>
						<a href="<c:url value='/resources/client/images/i7.jpg'/>">
							<img src="<c:url value='/resources/client/images/i7.jpg'/>" class="img-responsive" alt=""/>
							<div class="agile-figcap">
								<h4>KOCHI METRO</h4> 
							</div> 
						</a>
					</li> 
				</ul>
				<div class="clearfix"> </div>  
			</div> 
			<script type="text/javascript" src="js/simple-lightbox.min.js"></script>
			<script>
				$(function(){
					var gallery = $('.w3gallery-grids li a').simpleLightbox({navText:['&lsaquo;','&rsaquo;']});
				});
			</script>   
		</div>
	</div> 
	<!-- //gallery -->   
	<!-- subscribe -->
	<div class="subscribe jarallax">
		<div class="container">
			<div class="agileits-title title2">
				<h3>Subscribe</h3>
			</div>   
	
			<form>
				<input type="email" name="email" placeholder="Email Address" class="user" required="">
				<input type="submit" value="Subscribe">
			</form>
			<p class="spam">KOCHI METRO</p>
		</div>
	</div>