<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="banner-text agileinfo"><center><h1>PROVIDING FAST AND COMFORTABLE SERVICE<h1></center> 
<div class="container">
					<div class="flexslider">
			
							
					</div> 	 
				</div>
			</div>
			<!-- //banner-text -->  
			<!-- arrow bounce -->
		
			<div class="agileits-arrow bounce animated"><a href="#welcome" class="scroll"><i class="fa fa-angle-down" aria-hidden="true"></i></a></div>
			<!-- //arrow bounce -->  
		</div>
	</div>
	<!-- //banner --> 
	<!-- welcome -->
	<div class="welcome" id="welcome">
		<div class="container">   
			<div class="agileits-title">
				<h3>Who We Are</h3>
			</div> 
			<div class="welcomerow-agileinfo">
				<h5>KOCHI METRO</h5>
				<p>Kochi metro is India's first metro  service for a three tier city and is built at an approximate cost of Rs. 60000 crore.The first phase of kochi metro runs from Palarivattom to Aluva.This is an integrated service which provide all information about the metro rail and its routes for public. The proposed system is a web based application which provides information regarding timings, routes, fair. This system manages public feedback about services through its complaint management system. This system also contains an online ticket recharge module where users can recharge their smart cards online through the site. There is also an admin module where admin can add stations, trains, routes and also update the fairs. The admin is a panel consisting of a group of authorized persons.
The objectives of the projectare as follows: 
Users can register complaints through the site. 
User login page where users can recharge tickets online. 
Users can view metro timetable. 
User can also view the fair details and the route map. 
An admin login page where admin can add stations, trains,routes , update fairs and even add a new admin.
</p>
			</div> 
			<div class="w3-welcome-grids">
				<div class="col-md-3 col-xs-6 w3l-welcome-grid"> 
					<img src="<c:url value='/resources/client/images/g9.gif'/>" alt=" " class="img-responsive"/>   
					<div class="agileits-welcome-info">
						<h4>Kochi Metro</h4>
						<p>The Kochi Metro system is an urban Mass Rapid Transit System (MRTS) that is being built to serve Kochi, the commercial capital of Kerala. Construction for its 25.612 km Phase 1A from Aluva to Pettah with 22 stations started.</p>
					</div>
				</div>
				<div class="col-md-3 col-xs-6 w3l-welcome-grid">
					<img src="<c:url value='/resources/client/images/g4.gif'/>" alt=" " class="img-responsive"/>   
					<div class="agileits-welcome-info">
						<h4>Kochi Metro </h4>
						<p>In 2014, a 1.92 km eastward extension to Tripunithura from Pettah was approved to be included in Phase 1. Construction on it will begin once road widening along the route is completed.</p>
					</div>
				</div>
				<div class="col-md-3 col-xs-6 w3l-welcome-grid">
					<img src="<c:url value='/resources/client/images/g7.gif'/>" alt=" " class="img-responsive"/>   
					<div class="agileits-welcome-info">
						<h4>Kochi Metro</h4>
						<p>Operational: 18.2 km | Under Construction:  9.31 km | Approved: 11.2 km (Phase 1B) </p>
					</div>
				</div>
				<div class="col-md-3 col-xs-6 w3l-welcome-grid">
					<img src="<c:url value='/resources/client/images/g5.gif'/>" alt=" " class="img-responsive"/>   
					<div class="agileits-welcome-info">
						<h4>Kochi Metro</h4>
						<p>Kochi Metro Rail Time Table

Kochi Metro is likely to available for every five minutes from 6 Am in the morning until 11 Pm in the night. We will update the precise timings of Kochi Metro once the services start commencing.
</p>
					</div>
				</div>
				<br>
				<br><a href="more.html"><br>MORE</a>
				<div class="clearfix"> </div>
			</div> 
		</div>
	</div>
	<!-- //welcome -->
	<!-- services --><div class="subscribe jarallax">
		<div class="container">
			<div class="agileits-title title2">
			
			</div> 
<section id="w">			
			<div class="services">
		<div class="container">
		
			<div class="agileits-title">
				<h3>Services</h3>
			</div> 
<center>			
			<div class="services-w3lsrow">
				<div class="col-md-3 col-sm-3 col-xs-6 services-grids">  
					<i class="fa fa-gears icon" aria-hidden="true"><h4> STATIONS</h4>
							KALOOR-
							EDAPPALLY-
	ALUVA<a href="km.html"><br>
<br><center>					KOCHI METRO FOR MORE DETAILS</center></a></i> 
					
					
					
					
				</div>
				<div class="services-w3lsrow">
				<div class="col-md-3 col-sm-3 col-xs-6 services-grids">  
					<i class="fa fa-gears icon" aria-hidden="true"><h4> DISTANCE</h4>
							KALOOR-EDAPPALLY-4.7km
							EDAPPALLY-ALUVA-13.3km
	<a href="km.html"><br>
<br><center>					KOCHI METRO FOR MORE DETAILS</center></a></i> 
					
					
					
					
				</div>
				<div class="services-w3lsrow">
				<div class="col-md-3 col-sm-3 col-xs-6 services-grids">  
					<i class="fa fa-gears icon" aria-hidden="true"><h4> COST</h4>
							KALOOR-EDAPPALLY-Rs 20
							EDAPPALLY-ALUVA-Rs 70
	<a href="km.html"><br>
<br><center>					KOCHI METRO FOR MORE DETAILS</center></a></i> 
					
					
					
					
				</div>
				</div>
				<div class="services-w3lsrow">
				<div class="col-md-3 col-sm-3 col-xs-6 services-grids">  
					<i class="fa fa-gears icon" aria-hidden="true"><h4> BOOKING TIME</h4>
						6.00am-11.00pm
		
						
	<a href="km.html"><br>
<br><center>					KOCHI METRO FOR MORE DETAILS</center></a></i> 
					
					
					
					
				</div>
				</div></center>
				</div>
				<div class="clearfix"> </div>
				</section>
			</div>	 
		</div>
	</div>	
			
		
		</div>
	</div>
	
	<!-- //services -->	
	<!-- testimonials -->
	<section id="testimonials">
	<div class="testimonials">
		<div class="container">  
			<div class="agileits-title">
				<h3>Testimonials</h3>
			</div>  
			<div class="w3_testimonials_grids">
				<section class="slider">
					<div class="flexslider1">
						<ul class="slides">
							<li>	
								<div class="w3_testimonials_text">
									<img src="<c:url value='/resources/client/images/t4.jpg'/>" alt=" " class="img-responsive" />
									<h4 class="agiletext-style"><i>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil 
										impedit quo minus id quod maxime placeat facere possimus, omnis voluptas.</i></h4>
									<h5>John Frank</h5>
									<p>Transport Agent</p>
								</div>
							</li>
							<li>	
								<div class="w3_testimonials_text">
									<img src="<c:url value='/resources/client/images/t2.jpg'/>" alt=" " class="img-responsive" />
									<h4 class="agiletext-style"><i>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil 
										impedit quo minus id quod maxime placeat facere possimus, omnis voluptas.</i></h4>
									<h5>Michael Doe</h5>
									<p>Transport Agent</p>
								</div>
							</li>
							<li>	
								<div class="w3_testimonials_text">
									<img src="<c:url value='/resources/client/images/t5.jpg'/>" alt=" " class="img-responsive" />
									<h4 class="agiletext-style"><i>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil 
										impedit quo minus id quod maxime placeat facere possimus, omnis voluptas.</i></h4>
									<h5>Thomas Carl</h5>
									<p>Transport Agent</p>
								</div>
							</li>
						</ul>
					</div>
				</section> 
			</div>
		</div>
	</div>
	</section>
	<!-- //testimonials -->
	<!-- subscribe -->
	<div class="subscribe jarallax">
		<div class="container">
			<div class="agileits-title title2">
				<h3>Subscribe</h3>
			</div>   
			
			<form>
				<input type="email" name="email" placeholder="Email Address" class="user" required="">
				<input type="submit" value="Subscribe">
			</form>
			<p class="spam">KOCHI METRO</p>
		</div>
	</div>
	<!-- //subscribe -->
	<!-- features -->
	<div class="features">
		<div class="container">   
			<div class="wthree-features-row">
				<div class="col-md-6 features-w3grid"> 
					<div class="features-row2"> 
						<div class="features-w3lleft">  
							<h4>New User</h4>
							<p>Register here to become a member</p>
						</div>
						<div class="features-w3lright">
							<a href="#" class="w3ls-login" data-toggle="modal" data-target="#myModal2">Register</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-6 features-w3grid">  
					<div class="features-row2"> 
						<div class="features-w3lleft">  
							<h4>Already a Member </h4>
							<p>Please sign in here</p>
						</div>
						<div class="features-w3lright">
							<a href="#" class="w3ls-login" data-toggle="modal" data-target="#myModal1">Login</a>
						</div>
						<div class="clearfix"> </div>
					</div> 
				</div> 
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //features -->
	<!-- login modal -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModal1" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
					<img src="<c:url value='/resources/client/images/img5.png'/>" class="img-responsive login-img" alt=""/>
					<h4 class="modal-title">Don't Wait, Login now!</h4>
				</div>
				<div class="modal-body modal-body-sub"> 
					<h3>Login to your account</h3>
					<div class="register">
						<form action="#" method="post">			
							<input name="Email" placeholder="Email Address" type="text" required="">						
							<input name="Password" placeholder="Password" type="password" required="">										
							<div class="sign-up">
								<input type="submit" value="Login"/>
							</div>
						</form>
					</div>  
				</div>
			</div>
		</div>
	</div> 
	<!-- //login modal -->	
	<!-- register modal -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModal2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
					<img src="<c:url value='/resources/client/images/img5.png'/>" class="img-responsive login-img" alt=""/>
					<h4 class="modal-title">Don't Wait, Register now!</h4>
				</div>
				<div class="modal-body modal-body-sub"> 
					<div class="modal_body_left modal_body_left1"> 
						<h3>Register to your account</h3>
						<div class="register">
							<form action="#" method="post">			
								<input placeholder="Name" name="Name" type="text" required="">
								<input placeholder="Email Address" name="Email" type="email" required="">	
								<input placeholder="Password" name="Password" type="password" required="">	
								<input placeholder="Confirm Password" name="Password" type="password" required="">
								<div class="sign-up">
									<input type="submit" value="Register"/>
								</div>
							</form>
						</div> 		 
					</div>  
				</div>
			</div>
		</div>
	</div> 