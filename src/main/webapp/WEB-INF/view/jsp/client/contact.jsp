<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="banner-text agileinfo about-bnrtext"> 
				<div class="container">
					<h2><a href="index.html">Home</a> / Contact</h2> 
				</div>
			</div>
			<!-- //banner-text -->   
		</div>
	</div>
	<!-- //banner --> 
	<!-- contact -->
	<div class="contact agileits">
		<div class="container">  
			<div class="agileits-title">
				<h3>Contact Us</h3>
			</div>  
			<div class="contact-agileinfo">
				<div class="col-md-7 contact-form wthree">
					<form action="#" method="post">
						<input type="text" name="Name" placeholder="Name" required="">
						<input class="email" type="email" name="Email" placeholder="Email" required="">
						<textarea placeholder="Message" name="Message" required=""></textarea>
						<input type="submit" value="SUBMIT">
					</form>
				</div>
				<div class="col-md-4 contact-right wthree">
					<div class="contact-text w3-agileits">
						<h4>GET IN TOUCH :</h4>
						<p><i class="fa fa-map-marker"></i> Broome St, NY 10002, Canada. </p>
						<p><i class="fa fa-phone"></i> Telephone : +00 111 222 3333</p>
						<p><i class="fa fa-fax"></i> FAX : +1 888 888 4444</p>
						<p><i class="fa fa-envelope-o"></i> Email : <a href="mailto:example@mail.com">mail@example.com</a></p> 
					</div> 
				</div> 
				<div class="clearfix"> </div>	
			</div>
		</div>
	</div>
	<!-- //contact -->  
	<!-- map -->
	<div class="map w3layouts"> 
	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15651.201201507656!2d75.7815777!3d11.2760853!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1caace711d740b57!2sSANESQUARE+Technologies+LLP!5e0!3m2!1sen!2sin!4v1547048071818" ></iframe> 
	</div>
	<!-- //map -->   
	<!-- subscribe -->
	<div class="subscribe jarallax">
		<div class="container">
			<div class="agileits-title title2">
				<h3>Subscribe</h3>
			</div>   
			
			<form>
				<input type="email" name="email" placeholder="Email Address" class="user" required="">
				<input type="submit" value="Subscribe">
			</form>
			<p class="spam">Kochi Metro</p>
		</div>
	</div>