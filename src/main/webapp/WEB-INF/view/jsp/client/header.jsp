<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>KOCHI METRO</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Passengers Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript">
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 
</script>
<!-- Custom Theme files -->
<link href="<c:url value='/resources/client/css/bootstrap.css'/>"
	type="text/css" rel="stylesheet" media="all">
<link href="<c:url value='/resources/client/css/style.css'/>"
	type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet"
	href="<c:url value='/resources/client/css/flexslider.css'/>"
	type="text/css" media="screen" />
<!-- flexslider-CSS -->
<link href="<c:url value='/resources/client/css/font-awesome.css'/>"
	rel="stylesheet">
	<link href='<c:url value='/resources/client/css/simplelightbox.min.css'/>' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
<!-- //Custom Theme files -->
<!-- web-fonts -->
<link
	href="//fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700"
	rel="stylesheet">
<link
	href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
	rel="stylesheet">
<!-- //web-fonts -->
</head>
<body>
	<!-- banner -->
	<div class="agileits-banner">
		<div class="bnr-agileinfo">
			<!-- navigation -->
			<div class="top-nav w3-agiletop">
				<div class="container">
					<div class="navbar-header w3llogo">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<h1>
							<a href="index.html">KochiMetro</a>
						</h1>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">
						<div class="w3menu navbar-right">
							<ul class="nav navbar">
								<li><a href="index.html" class="active">Home</a></li>
								<li><a href="about.htm">About</a></li>
								<li><a href="service.htm">Services</a></li>
								<li><a href="gallery.htm">Gallery</a></li>
								<li><a href="#">Career</a></li>
								<li><a href="contact.htm">Contact</a></li>
								<li><a href="#">SignIn</a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>