<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!-- //navigation -->
			<!-- banner-text -->
			<div class="banner-text agileinfo about-bnrtext"> 
				<div class="container">
					<h2><a href="index.html">Home</a> / SIGIN/SIGNUP</h2> 
				</div>
			</div>
			<!-- //banner-text -->   
		</div>
	</div>
	<!-- //banner --> 
	<!-- welcome -->
	
	<!-- //team --> 
	<!-- subscribe -->
	
	<!-- //subscribe -->
	<!-- features -->
	<div class="features">
		<div class="container">   
			<div class="wthree-features-row">
				<div class="col-md-6 features-w3grid">  
					<div class="features-row2"> 
						<div class="features-w3lleft">  
							<h4>New User</h4>
							<p>Register here to become a member</p>
						</div>
						<div class="features-w3lright">
							<a href="#" class="w3ls-login" data-toggle="modal" data-target="#myModal2">Register</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
				<div class="col-md-6 features-w3grid">  
					<div class="features-row2"> 
						<div class="features-w3lleft">  
							<h4>Already a Member </h4>
							<p>Please sign in here</p>
						</div>
						<div class="features-w3lright">
							<a href="#" class="w3ls-login" data-toggle="modal" data-target="#myModal1">Login</a>
						</div>
						<div class="clearfix"> </div>
					</div> 
				</div> 
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //features -->
	<!-- login modal -->
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModal1" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
					
					<h4 class="modal-title">Don't Wait, Login now!</h4>
				</div>
				<div class="modal-body modal-body-sub"> 
					<h3>Login to your account</h3>
					<div class="register">
						<form action="pages/login_action.jsp" method="post">			
							<input name="Email" placeholder="Email Address" type="Email" required="">						
							<input name="Password" placeholder="Password" type="password" required="">										
							<div class="sign-up">
								<input type="submit" value="Login"/>
							</div>
						</form>
					</div>  
				</div>
			</div>
		</div>
	</div> 
	<!-- //login modal -->	
	<!-- register modal -->
	<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModal2" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
					
					<h4 class="modal-title">Don't Wait, Register now!</h4>
				</div>
				<div class="modal-body modal-body-sub"> 
					<div class="modal_body_left modal_body_left1"> 
						<h3>Register to your account</h3>
						<div class="register">
							<form action="pages/register.jsp" method="post">			
								<input placeholder="Name" name="Name" type="text" required="" pattern="[a-zA-Z]{}" >
								<input placeholder="Email Address" name="Email" type="email" required="">	
								<input placeholder="PhoneNumber" name="Ph" type="text" required="" pattern="[0-9]{10}">	
								<input placeholder="Confirm Password" name="Password" type="password" required="" pattern=".{6,}">
								<div class="sign-up">
									<input type="submit" value="Register"/>
								</div>
							</form>
						</div> 		 
					</div>  
				</div>
			</div>
		</div>
	</div> 