<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/breakingNews.css'/>" />
<link type="text/css" rel="stylesheet"
	href="<c:url value='/resources/assets/css/validation/formValidation.css'/>" />
</head>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">Price Information</div>
						<div class="panel-body">
							<form:form action="addPrice.do" commandName="priceVo"
								method="POST" id="price_form">
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default erp-info">
											<div class="panel-heading panel-inf">Price Information</div>
											<div class="panel-body">
												<form:hidden path="id" />
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">From Station
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">To Station
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														
													</div>
												</div>
												<div class="form-group">
													<label for="inputPassword3" class="col-sm-3 control-label">Price
														<span class="stars">*</span>
													</label>
													<div class="col-sm-9">
														<form:input path="price"/>
													</div>
												</div>
												<div class="form-group">
													<button type="submit" 
														class="btn btn-primary erp-btn">Save</button>
												</div>

												<a href="newPrice.do">
													<button type="button" class="btn btn-primary erp-btn right">New
														Price</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${! empty msg }">
		<div class="notification-panl successMsg">
			<div class="notfctn-cntnnt">${msg }</div>
			<span class="close-msg"><i class="fa fa-times"></i></span>
		</div>
	</c:if>
	<script src="<c:url value='/resources/assets/js/app/jQuery.js' />"></script>
	<script src="<c:url value='/resources/assets/js/jquery.growl.js' />"></script>
	<script src="<c:url value='/resources/assets/js/sample.js' />"></script>
	<script src="<c:url value='/resources/assets/js/rainbow.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/bootstrap.min.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/formValidation.js' />"></script>
	<script type="text/javascript"
		src="<c:url value='/resources/assets/js/validation/bootstrap.js' />"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			changeDocumentTitle('settingsli', 'User');
		});

		function validation() {
			var flag = true;
			if ($("#usrnm").val() == "") {
				alert("please enter your name");
				flag = false;
			}
			if ($("#em").val() == "") {
				alert("please enter your Email");
				flag = false;
			}
			if ($("#pass").val() != $("#conpass").val()) {
				alert("password do not match");
				flag = false;
			}
			if (flag) {
				//submit
				alert("successfull");
			}
		}
	</script>
</body>
</html>