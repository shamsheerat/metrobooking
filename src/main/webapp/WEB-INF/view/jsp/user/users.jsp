<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>metroBooking</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" />
<link href='<c:url value="/resources/asset/css/bootstrap.css"></c:url>'
	rel='stylesheet' type='text/css' />
<link href='<c:url value="/resources/asset/css/style.css"></c:url>'
	rel="stylesheet" type="text/css" media="all" />

<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<body>
	<div class="purchase_home">
		<div class="warper container-fluid">

			<div class="row">


				<div class="col-md-12">

					<div class="panel panel-default erp-panle">
						<div class="panel-heading  panel-inf">
							Users
						</div>
						<div class="panel-body">
							<div id="user_tbl">
								<table cellpadding="0" cellspacing="0" border="0"
									class="table table-striped table-bordered erp-tbl"
									id="basic-datatable">
									<thead>
										<tr>
											<th width="5%">#</th>
											<th width="45%">Username</th>
											<th width="45%">Email</th>
											<th width="5%">Edit</th>
											<th width="5%">Delete</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${users }" var="usr" varStatus="ss">
											<tr>
												<td>${ss.count }</td>
												<td>${usr.userName }</td>
												<td>${usr.email }</td>
												<td><a href="editUser.do?id=${usr.id }"><i class="fa fa-pencil"/></a></td>
												<td><a href="deleteUser.do?id=${usr.id }"><i class="fa fa-times"/></a></td>
											</tr>
										</c:forEach>

									</tbody>
								</table>
								<a href="addUser.do"><button type="button" class="btn btn-primary">New User</button></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script
			src='<c:url value="/resources/asset/js/jquery.min.js"></c:url>'></script>
		<script src="<c:url value='/resources/assets/metro/js/vp.js'/>"></script>
		<script>
			$(document).ready(function() {
				console.log('dd');

			});
		</script>
</body>
</html>