<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>metroBooking</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" />
<link href='<c:url value="/resources/asset/css/bootstrap.css"></c:url>' rel='stylesheet' type='text/css' />
<link href='<c:url value="/resources/asset/css/style.css"></c:url>' rel="stylesheet" type="text/css" media="all" />

<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<body>

	<div class="header text-center">
		<div class="container">
			<div class="logo">
				<a href="signout"><span><img src='resources/assets/images/metro.png' /></span></a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="navigation-strip effect2">
			<div class="navigation">
				<div class="top-menu">
					<span class="menu"> </span>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="content">
			<div class="banner text-center">
			<a class="log_out" href="signout"><input type="button"  id="logOut" value="Log Out" /></a>
				<div class="banner-head">
				</div>
			</div>
		</div>
	</div>
	<script src='<c:url value="/resources/asset/js/jquery.min.js"></c:url>'></script>
</body>
</html>