/**
 * @author Shamsheer & Vinutha
 * @since 4-August-2015
 */
$(document).ready(function() {
	$(".ledgDate").on("change",function(){
		$("#send_video_frm").formValidation('revalidateField', "expiryDate");
	});
	
	$("#prdct_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			productName : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			},
			unit : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Unit is required'
					}
				}
			},
			description : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Description is required'
					}
				}
			},
			rate : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Rate is required'
					},
					regexp : {
						regexp : /^((\d+)((\.\d{1,2})?))$/i,
						message : 'Enter a valid decimal place'
					}
				}
			}
		}
	});

	$("#company-form").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			companyName : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Name is required'
					}
				}
			},
			tinNumber : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : ' is required'
					}
				}
			},
			cstNumber : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : ' is required'
					}
				}
			},
			ssiNumber : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : ' is required'
					}
				}
			},
			officePhone : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Phone is required'
					}
				}
			},
			residencePhone : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Phone is required'
					}
				}
			},
			fax : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Fax is required'
					}
				}
			},
			website : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Website is required'
					}
				}
			},
			email : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'E-mail is required'
					}
				}
			},
			addressLine : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'Address is required'
					}
				}
			}
		}
	});

	$("#qtn_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			email : {
				row : '.col-sm-9',
				validators : {
					regexp : {
						regexp : '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message : 'The value is not a valid email address'
					}
				}
			}
		}
	});

	$("#send_video_frm").formValidation({
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			to : {
				row : '.col-sm-9',
				validators : {
					regexp : {
						regexp : '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message : 'The value is not a valid email address'
					},
					notEmpty : {
						message : 'E-mail is required'
					}
				}
			},
			companyId : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'required'
					}
				}
			},
			noOfTimes : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'required'
					}
				}
			},
			customer : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'required'
					}
				}
			},
			subject : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'required'
					}
				}
			},
			expiryDate : {
				row : '.col-sm-9',
				validators : {
					notEmpty : {
						message : 'required'
					},
					date : {
						message : 'The  date is not valid. ',
						format : 'DD/MM/YYYY',
						min : 'today',
					}
				}
			}
		}
	});
});