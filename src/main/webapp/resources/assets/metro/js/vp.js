/**
 * js file for purchase
 * 
 * @since 30-July-2015
 */

$('.numbersonly').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++)
		a.push(i);

	if (!(a.indexOf(k) >= 0))
		e.preventDefault();

});

function test(){
	alert();
}

$('.numbersonly').bind("paste", function(e) {
	e.preventDefault();
});
$('.double').keypress(function(e) {
	var a = [];
	var k = e.which;

	for (i = 48; i < 58; i++) {
		a.push(i);
	}
	if (i = 46) {
		if ($(this).val().indexOf('.') == -1)
			a.push(i);
	}
	if (!(a.indexOf(k) >= 0))
		e.preventDefault();
	// alert($(this).val().indexOf('.'));
});
$('.double').bind("paste", function(e) {
	e.preventDefault();
});

$(".double").on("click", function() {
	this.select();
});

setTimeout(function() {
	$(".successMsg").fadeOut(600);
}, 3500);
$(".close-msg").click(function() {
	$(".successMsg").fadeOut(600);
});

/**
 * function to delete item
 * 
 * @param id
 */
function deleteThisUnit(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('unit');
	$("#id-hid").val(id);
}

/**
 * function to close confirm-delete pop
 */
function cancel() {
	$("#confirm-delete").css("display", "none");
	$("#id-hid").val('');
}

/**
 * function to confirm delete
 */
function deleteItem() {
	var item = $("#delete-item").val();
	if (item == 'product')
		deleteProduct($("#id-hid").val());
	else if (item == "company")
		deleteThisCompany($("#id-hid").val());
	$("#confirm-delete").css("display", "none");
}

function deleteThisProduct(id) {
	$("#confirm-delete").css("display", "block");
	$("#delete-item").val('product');
	$("#id-hid").val(id);
}

/**
 * function to delete product
 * 
 * @param id
 */
function deleteProduct(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		url : "deleteProduct.do",
		cache : false,
		async : true,
		data : {
			"id" : id
		},
		success : function(response) {
			if (response != 'ok') {
				$(".notfctn-cntnnt").text(response);
				$("#erro-msg").css("display", "block");
				$("#load-erp").css("display", "none");
			} else {
				$("#notfctn-cntnnt").text(response);
				$("#successMsg").css("display", "block");
			}
			loadProductTable();
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

/**
 * function to load product list
 */
function loadProductTable() {
	$.ajax({
		type : "GET",
		url : 'productList.do',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		success : function(response) {
			$("#product_tbl_div").html(response);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);
}

/**
 * Add bill Page
 */

getProductDetails = function() {
	var ids = [];
	$(".prodctId").each(function() {
		ids.push(this.value);
	});
	var vo = new Object();
	vo.ids = ids;
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "POST",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		data : JSON.stringify(vo),
		async : true,
		url : "productPopUp.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});

}

closePdtpo = function() {
	$("#load-erp").css("display", "block");
	var ids = [];

	$("input[name=product]:checked",
			$('#basic-datatables').dataTable().fnGetNodes()).each(function() {
		ids.push(this.value);
	});

	/*
	 * $("input[name=product]").each(function() { if (this.checked)
	 * ids.push(this.value); });
	 */

	$("input[name=selectedProduct]:checked",
			$('#basic-datatables').dataTable().fnGetNodes()).each(function() {
		ids.push(this.value);
	});

	/*
	 * $("input[name=selectedProduct]").each(function() { if (this.checked)
	 * ids.push(this.value); });
	 */
	var vo = new Object();
	vo.ids = ids;
	var count = $(".prdct_tbl tr").size();
	count = parseFloat(count) - parseFloat(1);
	vo.rowCount = count;
	$.ajax({
		type : 'POST',
		dataType : "html",
		contentType : "application/json; charset=utf-8",
		cache : false,
		async : true,
		data : JSON.stringify(vo),
		url : "findProducts.do",
		success : function(response) {
			$(".prdct_tbl tr").remove();
			$(".prdct_tbl").append(response);
			findQuotationTotal();
			// changeComName();
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
			$("#load-erp").css("display", "none");
		}
	});
	$("#pdtPop").fadeOut("600");
}

cancelPdtpo = function() {
	$("#pdtPop").fadeOut("600");
}

$('.proTable tr').click(function(event) {
	if (event.target.type !== 'checkbox') {
		$(':checkbox', this).trigger('click');
	}
});

/**
 * function to calculate product row total
 * 
 * @param id
 */
function calculateProductSum(id) {
	var qty = $("#product_qty" + id).val();
	var rate = $("#product_rt" + id).val();
	var total = parseFloat(qty) * parseFloat(rate).toFixed(2);
	$("#prod_tot" + id).text(total);
	findQuotationTotal();
}

/**
 * function to calculate grand total
 */
function findQuotationTotal() {
	var total = "0.00";
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			total = parseFloat(total) + parseFloat($("#prod_tot" + id).text());
		}
	});
	$("#grandTotal").text(total);
}

function closeError() {
	$("#errror-msg").css("display", "none");
}

function createQuotationVo() {
	var vo = new Object();
	var itemVos = [];
	vo.grandTotal = $("#grandTotal").text();
	vo.companyId = $("#companyIdSelect").val();
	vo.isOrder = $("#isOrder").val();
	vo.id = $("#quotationId").val();
	vo.customerName = $("#qut_cust_nm").val();
	vo.email = $("#ref_mail").val();
	vo.customerDetails = $("#qut_cust_det").val();
	vo.date = $("#quot-date").val();
	vo.subDate = $("#quot-ref_date").val();
	vo.subRef = $("#qut_sub_numb").val();
	vo.referenceNumber = $("#ref_quot").text();
	vo.deliveryTime = $("#qut_dev_tim").val();
	$(".prdct_tbl tr").each(function() {
		var id = this.id;
		if (id != '') {
			var item = new Object();
			item.productId = id;
			item.productName = $("#prod_name" + id).text();
			item.description = $("#prod_desc" + id).val();
			item.total = $("#prod_tot" + id).text();
			item.quantity = $("#product_qty" + id).val();
			item.rate = $("#product_rt" + id).val();
			itemVos.push(item);
		}
	});
	vo.itemVos = itemVos;
	return vo;
}

/**
 * function to send quotation mail
 */
function sendQuotationMail() {
	$("#load-erp").css("display", "block");
	var flag = true;
	var msg;
	var vo = createQuotationVo();
	if (vo.companyId == "") {
		flag = false;
		msg = "Company is required.";
	} else if (vo.customerDetails == "") {
		flag = false;
		msg = "Customer details is required.";
	} else if (vo.date == "") {
		flag = false;
		msg = "Date is required.";
	} else if (vo.itemVos.length == 0) {
		flag = false;
		msg = "Products required.";
	} else if (vo.email == "") {
		flag = false;
		msg = "Email required.";
	} else if (vo.customerName == "") {
		flag = false;
		msg = "Name is required.";
	}

	if (flag) {
		$.ajax({
			type : "POST",
			cache : false,
			async : true,
			url : "sendQuotationMail.do",
			dataType : "json",
			contentType : "application/json;charset=utf-8",
			data : JSON.stringify(vo),
			success : function(response) {
				setTimeout(function() {
					$("#load-erp").css("display", "none");
				}, 1000);
				location.href("quotation.do?id=" + response
						+ "&msg=Your quotation has been saved and mailed to "
						+ vo.email);
			},
			error : function(requestObject, error, errorThrown) {
				$("#load-erp").css("display", "none");
				alert(errorThrown);
			}
		});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
		$("#load-erp").css("display", "none");
	}

}

/**
 * function to save quotation
 */
function saveQuotation() {
	$("#load-erp").css("display", "block");
	var flag = true;
	var msg;
	var vo = createQuotationVo();
	if (vo.companyId == "") {
		flag = false;
		msg = "Company is required.";
	} else if (vo.customerDetails == "") {
		flag = false;
		msg = "Customer details is required.";
	} else if (vo.date == "") {
		flag = false;
		msg = "Date is required.";
	} else if (vo.itemVos.length == 0) {
		flag = false;
		msg = "Products required.";
	} else if (vo.email == "") {
		flag = false;
		msg = "Email required.";
	} else if (vo.customerName == "") {
		flag = false;
		msg = "Name is required.";
	}
	if (flag) {
		$
				.ajax({
					type : "POST",
					cache : false,
					async : true,
					url : "saveQuotation.do",
					dataType : "json",
					contentType : "application/json;charset=utf-8",
					data : JSON.stringify(vo),
					success : function(response) {
						if (response == 0) {
							$("#errror-msg").css("display", "block");
							$(".modal-body")
									.text("Oops.. something went wrong");
						} else {
							location.href = "quotation.do?id="
									+ response
									+ "&msg=Your quotation has been saved Successfully ";
						}
					},
					error : function(requestObject, error, errorThrown) {
						alert(errorThrown);
					}
				});
	} else {
		$("#errror-msg").css("display", "block");
		$(".modal-body").text(msg);
	}

	$("#load-erp").css("display", "none");
}

function deleteCompany(id) {
	$("#confirm-delete").css("display", "block");
	$("#id-hid").val(id);
	$("#delete-item").val('company');
}

function deleteThisCompany(id) {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		cache : false,
		async : true,
		url : "deleteThisCompany.do",
		data : {
			"id" : id
		},
		dataType : "json",
		contentType : "application/json;charset=utf-8",
		success : function(response) {
			var msg = "";
			if (response == true) {
				msg = "Company Deleted Successfully.";
			} else {
				msg = "Company Delete Failed.";
			}
			location.href("settings.do?msg=" + msg);
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
	$("#load-erp").css("display", "none");
}

filterPendingQuotes = function() {
	$("#totl_amnt").text("0.00");
	var vo = new Object();
	vo.startDate = $("#peQotStrt").val();
	vo.endDate = $("#peQotEnd").val();
	vo.companyIds = $("#companyIdSelect").val();

	if (vo.startDate.length > 0 && vo.endDate.length > 0) {
		if (process(vo.startDate) > process(vo.endDate)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "POST",
				url : "quotationList.do",
				cache : false,
				async : true,
				data : JSON.stringify(vo),
				contentType : "application/json;charset=utf-8",
				dataType : "html",
				success : function(response) {
					$("#quot_tbl_div").html(response);
					$("#load-erp").css("display", "none");
					calculateGrandTotal();
				},
				error : function(requestObject, error, errorThrown) {
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
				}
			});
		}
	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}

function process(date) {
	var parts = date.split("/");
	return new Date(parts[2], parts[1] - 1, parts[0]);
}

calculateGrandTotal = function() {
	var total = 0;
	$(".qot-tabl tr").each(function() {
		var content = this.cells[4].innerHTML;
		if (content != "Total") {
			total = parseFloat(total) + parseFloat(content);
		}
	});
	$("#totl_amnt").text(total.toFixed(2));
}

calculateGrandTotalOrders = function() {
	var total = 0;
	$(".ord-tbl tr").each(function() {
		var content = this.cells[4].innerHTML;
		if (content != "Total") {
			total = parseFloat(total) + parseFloat(content);
		}
	});
	$("#totl_amnt_ordr").text(total.toFixed(2));
}

filterOrder = function() {
	$("#totl_amnt_ordr").text("0.00");
	var vo = new Object();
	vo.startDate = $("#ordrStart").val();
	vo.endDate = $("#ordrEnd").val();
	vo.companyIds = $("#companyIdSelect").val();
	if (vo.startDate.length > 0 && vo.endDate.length > 0) {
		if (process(vo.startDate) > process(vo.endDate)) {
			$(".modal-body").text("Please Select valid date range..");
			$("#errror-msg").css("display", "block");
		} else {
			$("#load-erp").css("display", "block");
			$.ajax({
				type : "POST",
				url : "orderList.do",
				cache : false,
				async : true,
				data : JSON.stringify(vo),
				contentType : "application/json;charset=utf-8",
				dataType : "html",
				success : function(response) {
					$("#ordr_tbl_div").html(response);
					$("#load-erp").css("display", "none");
					calculateGrandTotal();
				},
				error : function(requestObject, error, errorThrown) {
					alert(errorThrown);
					$(".notfctn-cntnnt").text(errorThrown);
					$("#success-msg").css("display", "block");
					$("#load-erp").css("display", "none");
				}
			});
		}

	} else {
		$(".modal-body").text("Date fields cannot be empty..");
		$("#errror-msg").css("display", "block");
	}
}
closeError = function() {
	$("#errror-msg").fadeOut("600");
}
printQuotation = function() {

	var id = $("#quotationId").val();
	if (id.length == 0) {
		$(".modal-body").text("Please Save Quotation..");
		$("#errror-msg").css("display", "block");
	} else {
		window.location = 'quotationPrint.do?id=' + id;
	}

}
quotationPdf = function() {
	var id = $("#quotationId").val();
	if (id.length == 0) {
		$(".modal-body").text("Please Save Quotation..");
		$("#errror-msg").css("display", "block");
	} else {
		window.location = 'quotationPdf.do?id=' + id;
	}
}

quotationHistory = function() {
	var start = $("#peQotStrt").val();
	var end = $("#peQotEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'quotationHistory.do?startDate=' + start + '&endDate='
			+ end;
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);

}

orderHistory = function() {
	var start = $("#ordrStart").val();
	var end = $("#ordrEnd").val();
	$("#load-erp").css("display", "block");
	window.location = 'orderHistory.do?startDate=' + start + '&endDate=' + end;
	setTimeout(function() {
		$("#load-erp").css("display", "none");
	}, 1000);

}
var label = '"' + $("#company").val() + '"';
var prefix = '';
function changeComName() {

	$("#load-erp").css("display", "block");
	getCodeForCOmpany($("#companyIdSelect").val(), $("#isOrder").val());
	/*
	 * var oldLabel = label; // label = '"' + $("#companyIdSelect
	 * option:selected").text() + '"'; var id = $("#companyIdSelect").val(); if
	 * (id != '') { $.ajax({ type : "GET", cache : false, async : true, url :
	 * "getProductPrefixForCompany.do", data : { "id" : id }, success :
	 * function(r) { prefix = r; label = '"' + prefix + '"'; $(".prdct_tbl
	 * tr").each(function(i, item) { var id = this.id; if (id != '') { var des =
	 * $("#prod_desc" + id).text(); if (des.indexOf(oldLabel) >= 0) { var
	 * splittedDes = des.split(oldLabel); des = splittedDes[1]; } if (label !=
	 * '"-Select Company-"') var updDes = label + ' ' + des; else var updDes =
	 * des; $("#prod_desc" + id).text(updDes); } }); }, error :
	 * function(requestObj, err, errorThro) { alert(errorThro); } }); }
	 */
	$("#load-erp").css("display", "none");

}

function getCodeForCOmpany(id, isOrder) {
	if (id != '') {
		$.ajax({
			type : "GET",
			cache : false,
			async : true,
			url : "getCodeForCompany.do",
			data : {
				"id" : id,
				"isOrder" : isOrder
			},
			success : function(r) {
				$("#ref_quot").text(r);
			},
			error : function(requestObj, err, errorThro) {
				alert(errorThro);
			}
		});
	} else {
		$("#ref_quot").text("");
	}
}

function findCustomers() {
	$("#load-erp").css("display", "block");
	$.ajax({
		type : "GET",
		contentType : "application/json; charset=utf-8",
		dataType : "html",
		cache : false,
		async : true,
		url : "findCustomers.do",
		success : function(response) {
			$("#pdtPop").html(response);
			$("#pdtPop").css("display", "block");
			$("#load-erp").css("display", "none");
		},
		error : function(requestObject, error, errorThrown) {
			alert(errorThrown);
		}
	});
}

function selectThisCustomer() {
	$("input[name=cust]:checked",
			$('#basic-datatables').dataTable().fnGetNodes()).each(function() {
		var cust = this.value;
		var custArray = cust.split('!@#');
		$("#qut_cust_nm").val(custArray[0]);
		$("#qut_cust_det").val(custArray[1]);
		$("#ref_mail").val(custArray[2]);
		$("#pdtPop").fadeOut("600");
	});
}