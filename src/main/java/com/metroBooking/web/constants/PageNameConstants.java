package com.metroBooking.web.constants;

public interface PageNameConstants {

	String SIGN_IN = "login";
	String HOME_PAGE = "home";
	String DEMO="login";
	
	String USER_HOME="users";
	String ADD_USERS="adduser";
	
	String STATION_HOME="stationHome";
	String ADD_STATION="addStation";
	
	String SLOT_HOME="slotHome";
	String ADD_SLOT="addSlot";
	
	String PRICE_HOME="priceHome";
	String ADD_PRICE="addPrice";
	
//	Client Side
	
	String CLIENT_HOME="clientHome";
	String CLIENT_ABOUT="clientAbout";
	String CLIENT_GALLERY="clientGallery";
	String CLIENT_SERVICE="clientService";
	String CLIENT_CONTACT="clientContact";
}
