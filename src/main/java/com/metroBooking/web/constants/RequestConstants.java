package com.metroBooking.web.constants;
/**
 * 
 * @author Vinutha
 * @since 20-July-2015
 */
public interface RequestConstants {

	String LOGIN_URL="signin";
	String LOGOUT_URL="signout";
	String HOME_URL="homePage.do";
	String DEMO="demo";
	
	
	String USER_HOME="users.do";
	String USER_EDIT="editUser.do";
	String USER_DELETE="deleteUser.do";
	String USER_ADD="addUser.do";
	String USER_REGISTER="registerUser.do";
	
	String STATION_HOME="station.do";
	String STATION_EDIT="editStation.do";
	String STATION_DELETE="deleteStation.do";
	String STATION_ADD="addStation.do";
	String NEW_STATION="newStation.do";
	
	
	String SLOT_HOME="slot.do";
	String SLOT_EDIT="editSlot.do";
	String SLOT_DELETE="deleteSlot.do";
	String SLOT_ADD="addSlot.do";
	String NEW_SLOT="newSlot.do";
	
	
	String PRICE_HOME="price.do";
	String PRICE_EDIT="editPrice.do";
	String PRICE_DELETE="deletePrice.do";
	String PRICE_ADD="addPrice.do";
	String PRICE_SLOT="newPrice.do";
//	Client Side
	
	String CLIENT_HOME="home.htm";
	String CLIENT_ABOUT="about.htm";
	String CLIENT_GALLERY="gallery.htm";
	String CLIENT_SERVICE="service.htm";
	String CLIENT_CONTACT="contact.htm";
}
