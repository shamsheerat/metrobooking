package com.metroBooking.web.security;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;

import org.apache.http.auth.InvalidCredentialsException;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.metroBooking.web.service.UserService;
import com.metroBooking.web.vo.UserVo;

/**
 * 
 * @author Shamsheer
 * @since 30-June-2015
 */
@EnableTransactionManagement
public class SecureAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
	
	@Autowired
	private UserService userService;

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		return;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		String passWord = null;
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		passWord = (String) authentication.getCredentials();
		UserVo userVo=userService.getUser(username);
		if (!StringUtils.hasText(passWord)) {
			throw new BadCredentialsException("Please enter password");
		}
		String expectedPassword = DigestUtils.md5DigestAsHex(passWord.getBytes());

		if (!expectedPassword.equals(userVo.getPassword())) {
			throw new BadCredentialsException("Invalid credentials");
		}
		if (!StringUtils.hasText(expectedPassword)) {
			throw new BadCredentialsException(
					"No password for " + username + " set in database, contact administrator");
		}
		return new User(username, passWord, true, true, true, true, authorities);
	}

}
