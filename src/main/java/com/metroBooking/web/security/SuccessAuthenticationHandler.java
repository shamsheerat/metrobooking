package com.metroBooking.web.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.metroBooking.web.constants.RequestConstants;


/**
 * 
 * @author Shamsheer
 * @since 30-June-2015
 */
public class SuccessAuthenticationHandler implements AuthenticationSuccessHandler{

	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		response.sendRedirect(RequestConstants.HOME_URL+"?jsession=FA");
	}

}
