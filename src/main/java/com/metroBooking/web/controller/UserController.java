package com.metroBooking.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.metroBooking.web.constants.PageNameConstants;
import com.metroBooking.web.constants.RequestConstants;
import com.metroBooking.web.service.UserService;
import com.metroBooking.web.vo.UserVo;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = RequestConstants.USER_HOME)
	public String users(final Model model) {
		try {
			List<UserVo> userVos = userService.getAllUsers();
			model.addAttribute("users", userVos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.USER_HOME;
	}

	@RequestMapping(value = RequestConstants.USER_ADD)
	public String addUserPage(final Model model) {
		try {
			model.addAttribute("userBean", new UserVo());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_USERS;
	}

	@RequestMapping(value = RequestConstants.USER_EDIT)
	public String editUser(final Model model, @RequestParam(value = "id") Long id) {
		try {
			model.addAttribute("userBean", userService.getUser(id));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_USERS;
	}

	@RequestMapping(value = RequestConstants.USER_DELETE)
	public String deleteUser(final Model model, @RequestParam(value = "id", required = true) Long id) {
		try {
			userService.deleteUser(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.USER_HOME;
	}

	@RequestMapping(value = RequestConstants.USER_REGISTER, method = RequestMethod.POST)
	public String registerUser(final Model model, @ModelAttribute(value = "userBean") UserVo userVo) {
		try {
			if (userVo.getId() != null)
				userService.updateUser(userVo);
			else
				userService.saveUser(userVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_USERS;
	}
}
