package com.metroBooking.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.metroBooking.web.constants.PageNameConstants;
import com.metroBooking.web.constants.RequestConstants;
import com.metroBooking.web.service.StationService;
import com.metroBooking.web.vo.StationVo;

@Controller
public class StationController {
	
	@Autowired
	StationService stationService;
	
	@RequestMapping(value=RequestConstants.STATION_HOME)
	public String allStations(final Model model) {
		try {
			List<StationVo> vos=stationService.getAllstations();
			model.addAttribute("stationVos", vos);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.STATION_HOME;
	}
	
	@RequestMapping(value=RequestConstants.NEW_STATION)
	public String newStations(final Model model) {
		try {
			model.addAttribute("stationVo", new StationVo());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_STATION;
	}
	
	@RequestMapping(value=RequestConstants.STATION_ADD)
	public String addStations(final Model model,@ModelAttribute StationVo stationVo) {
		try {
			stationService.savestation(stationVo);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_STATION;
	}
	
	@RequestMapping(value=RequestConstants.STATION_EDIT)
	public String editStations(final Model model,
			@RequestParam("id")Long id) {
		try {
			model.addAttribute("stationVo", stationService.getstation(id));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_STATION;
	}
	
	@RequestMapping(value=RequestConstants.STATION_DELETE)
	public String deleteStations(final Model model,
			@RequestParam("id")Long id) {
		try {
			stationService.deletestation(id);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.STATION_HOME;
	}
	

}
