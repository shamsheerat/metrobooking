package com.metroBooking.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.metroBooking.web.constants.PageNameConstants;
import com.metroBooking.web.constants.RequestConstants;
import com.metroBooking.web.service.SlotService;
import com.metroBooking.web.vo.SlotVo;
import com.metroBooking.web.vo.StationVo;

@Controller
public class SlotController {
	
	@Autowired
	private SlotService slotService;
	
	@RequestMapping(value=RequestConstants.SLOT_HOME)
	public String allSlots(final Model model) {
		try {
			List<SlotVo> vos=slotService.getAllslots();
			model.addAttribute("slotVos", vos);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.SLOT_HOME;
	}
	
	@RequestMapping(value=RequestConstants.NEW_SLOT)
	public String newStations(final Model model) {
		try {
			model.addAttribute("slotVo", new SlotVo());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_SLOT;
	}
	
	@RequestMapping(value=RequestConstants.SLOT_ADD)
	public String addStations(final Model model,@ModelAttribute SlotVo slotVo) {
		try {
			slotService.saveslot(slotVo);;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_SLOT;
	}
	
	@RequestMapping(value=RequestConstants.SLOT_EDIT)
	public String editStations(final Model model,
			@RequestParam("id")Long id) {
		try {
			model.addAttribute("slotVo", slotService.getslot(id));
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return PageNameConstants.ADD_SLOT;
	}
	
	@RequestMapping(value=RequestConstants.SLOT_DELETE)
	public String deleteStations(final Model model,
			@RequestParam("id")Long id) {
		try {
			slotService.deleteslot(id);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:"+RequestConstants.SLOT_HOME;
	}
	

}



