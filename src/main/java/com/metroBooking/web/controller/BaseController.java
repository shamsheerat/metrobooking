package com.metroBooking.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.metroBooking.web.constants.PageNameConstants;
import com.metroBooking.web.constants.RequestConstants;
import com.metroBooking.web.logger.Log;

@Controller
public class BaseController {

	/**
	 * Log
	 */
	@Log
	private static Logger logger;

	@RequestMapping(value = RequestConstants.LOGIN_URL, method = RequestMethod.GET)
	public String loginPage(final Model model, @RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "invalid", required = false) String invalid) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {

			return "redirect:" + RequestConstants.HOME_URL;
		}
		if (error != null) {
			model.addAttribute("status", "Invalid Username or Password");
		}
		if (logout != null) {
			model.addAttribute("status", "You have been logged out successfully.");
		}
		if (invalid != null) {
			model.addAttribute("status", "Your Session Expired.Please Do Login.");
		}
		return PageNameConstants.SIGN_IN;
	}

	@RequestMapping(value = RequestConstants.HOME_URL, method = RequestMethod.GET)
	public String homePge(final Model model, HttpServletRequest request) {
		model.addAttribute("msg", request.getParameter("msg"));
		try {
			User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (Exception e) {
			logger.info("HOME_URL : ", e);
		}
		return PageNameConstants.HOME_PAGE;
	}

}
