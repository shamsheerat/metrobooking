package com.metroBooking.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.metroBooking.web.constants.PageNameConstants;
import com.metroBooking.web.constants.RequestConstants;
import com.metroBooking.web.service.UserService;
import com.metroBooking.web.vo.UserVo;

@Controller
public class DemoController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = RequestConstants.DEMO)
	public String demo() {
		try {
			System.out.println("Executing demo");
			UserVo userVo = new UserVo();
			userVo.setUserName("admin");
			userVo.setPassword("admin");
			userService.saveUser(userVo);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return PageNameConstants.DEMO;
	}
}
