package com.metroBooking.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.metroBooking.web.constants.PageNameConstants;
import com.metroBooking.web.constants.RequestConstants;

@Controller	
public class ClientController {
	
	@RequestMapping(value=RequestConstants.CLIENT_HOME)
	public String clientHome() {
		return PageNameConstants.CLIENT_HOME;
	}
	
	@RequestMapping(value=RequestConstants.CLIENT_ABOUT)
	public String clientAboutUs() {
		return PageNameConstants.CLIENT_ABOUT;
	}
	
	@RequestMapping(value=RequestConstants.CLIENT_CONTACT)
	public String clientContact() {
		return PageNameConstants.CLIENT_CONTACT;
	}
	
	@RequestMapping(value=RequestConstants.CLIENT_GALLERY)
	public String clientGallery() {
		return PageNameConstants.CLIENT_GALLERY;
	}
	
	@RequestMapping(value=RequestConstants.CLIENT_SERVICE)
	public String clientServices() {
		return PageNameConstants.CLIENT_SERVICE;
	}
}
