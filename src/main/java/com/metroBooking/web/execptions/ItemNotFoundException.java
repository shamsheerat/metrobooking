package com.metroBooking.web.execptions;

public class ItemNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1350499370235922058L;

	private String message = null;

	/**
	 * 
	 * @param message
	 */
	public ItemNotFoundException(String message) {
		super(message);
		this.message = message;
	}

	/**
	 * 
	 * @param cause
	 */
	public ItemNotFoundException(Throwable cause) {
		super(cause);
	}

	@Override
	public String toString() {
		return message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
