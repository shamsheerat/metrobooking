package com.metroBooking.web.execptions;

public class CustomException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6286054248617909298L;
	private String message;

	private CustomException() {
		// TODO Auto-generated constructor stub
	}

	public CustomException(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return this.message;
	}
	
	@Override
	public String getMessage() {
		return this.message;
	}
}
