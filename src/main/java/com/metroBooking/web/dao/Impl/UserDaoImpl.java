package com.metroBooking.web.dao.Impl;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.util.DigestUtils;

import com.metroBooking.web.dao.AbstractDao;
import com.metroBooking.web.dao.UserDao;
import com.metroBooking.web.entities.User;
import com.metroBooking.web.execptions.CustomException;
import com.metroBooking.web.vo.UserVo;

@Repository
public class UserDaoImpl extends AbstractDao implements UserDao {

	public void saveUser(UserVo vo) {
		if (this.getUser(vo.getUserName()) == null) {
			User
			user = new User();
			user.setUserName(vo.getUserName());
			user.setPassword(DigestUtils.md5DigestAsHex(vo.getPassword().getBytes()));
			user.setEmail(vo.getEmail());
			this.sessionFactory.getCurrentSession().save(user);
		} else {
			throw new CustomException("User already exists");
		}
	}

	public UserVo getUser(Long id) {

		User user = getUserObjectById(id);
		if (user != null) {
			UserVo userVo = new UserVo();
			userVo.setUserName(user.getUserName());
			userVo.setEmail(user.getEmail());
			userVo.setId(user.getId());
			return userVo;
		}
		return null;
	}

	public UserVo getUser(String userName) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("userName", userName));
		// select * from user where userName='userName'
		User user = (User) criteria.uniqueResult();
		if (user != null) {
			UserVo userVo = new UserVo();
			userVo.setUserName(user.getUserName());
			userVo.setPassword(user.getPassword());
			userVo.setId(user.getId());
			return userVo;
		}
		return null;
	}

	private User getUserObjectById(Long id) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Restrictions.eq("id", id));
		return (User) criteria.uniqueResult();
	}

	public void updateUser(UserVo vo) {
		User user = getUserObjectById(vo.getId());
		user.setUserName(vo.getUserName());
		if (vo.getPassword() != null && !vo.getPassword().isEmpty())
			user.setPassword(DigestUtils.md5DigestAsHex(vo.getPassword().getBytes()));
		user.setEmail(vo.getEmail());
		this.sessionFactory.getCurrentSession().save(user);
	}

	public List<UserVo> getAllUsers() {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(User.class);
		List<User> users = criteria.list();
		List<UserVo> userVos = new LinkedList<UserVo>();
		for (User user : users) {
			UserVo userVo = new UserVo();
			userVo.setUserName(user.getUserName());
			userVo.setEmail(user.getEmail());
			userVo.setId(user.getId());
			userVos.add(userVo);
		}
		return userVos;
	}

	public void deleteUser(Long id) {
		User user=getUserObjectById(id);
		if(user!=null) {
			this.sessionFactory.getCurrentSession().delete(user);
		}
	}

}
