package com.metroBooking.web.dao.Impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.metroBooking.web.dao.AbstractDao;
import com.metroBooking.web.dao.SlotDao;
import com.metroBooking.web.entities.Slot;
import com.metroBooking.web.vo.SlotVo;


@Repository
public class SlotDaoImpl extends AbstractDao implements SlotDao{
	
	
	
	public void saveslot(SlotVo vo){
		Long id=vo.getId();
		Slot slot;
		if(id==null)
			slot=new Slot();
		else
			slot=this.getSlotById(id);
		slot.setSlot(vo.getSlot());
		this.sessionFactory.getCurrentSession().save(slot);
	}
	
	public SlotVo getslot(Long id) {
		SlotVo vo=new SlotVo();
		Slot slot=this.getSlotById(id);
		vo.setId(slot.getId());
		vo.setSlot(slot.getSlot());
		return vo;
	}

	
	public List<SlotVo> getAllslots(){
		List<SlotVo> vos=new ArrayList<SlotVo>();
		List<Slot> slots=this.sessionFactory.getCurrentSession().createCriteria(Slot.class)
				.add(Restrictions.or(Restrictions.eq("isDeleted", false),Restrictions.isNull("isDeleted")))
				.list();
		for(Slot slot:slots) {
			SlotVo vo=new SlotVo();
			vo.setId(slot.getId());
			vo.setSlot(slot.getSlot());
			vos.add(vo);
		}
		return vos;
	}
	
	public void deleteslot(Long id) {
		Slot slot=this.getSlotById(id);
		slot.setIsDeleted(true);
		this.sessionFactory.getCurrentSession().save(slot);
		
	}
	
	
	public Slot getSlotById(Long id) {
		return (Slot) this.sessionFactory.getCurrentSession().createCriteria(Slot.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}
	
	

}
