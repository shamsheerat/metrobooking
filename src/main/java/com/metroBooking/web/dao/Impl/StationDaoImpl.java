package com.metroBooking.web.dao.Impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.metroBooking.web.dao.AbstractDao;
import com.metroBooking.web.dao.StationDao;
import com.metroBooking.web.entities.Station;
import com.metroBooking.web.service.StationService;
import com.metroBooking.web.vo.StationVo;


@Repository
public class StationDaoImpl extends AbstractDao implements StationDao{
	
	public void savestation(StationVo vo){
		Long id=vo.getId();
		Station station;
		if(id==null)
			station=new Station();
		else
			station=this.getStationById(id);
		station.setStationName(vo.getStation());
		station.setAddress(vo.getAddress());
		station.setContactNumber(vo.getContactNumber());
		station.setContactPerson(vo.getContactPerson());
		this.sessionFactory.getCurrentSession().save(station);
	}
	
	public StationVo getstation(Long id) {
		StationVo vo=new StationVo();
		Station station=this.getStationById(id);
		vo.setId(station.getId());
		vo.setStation(station.getStationName());
		vo.setContactPerson(station.getContactPerson());
		vo.setAddress(station.getAddress());
		vo.setContactNumber(station.getContactNumber());
		return vo;
	}

	
	public List<StationVo> getAllstations(){
		List<StationVo> vos=new ArrayList<StationVo>();
		List<Station> stations=this.sessionFactory.getCurrentSession().createCriteria(Station.class)
				.add(Restrictions.or(Restrictions.eq("isDeleted", false),Restrictions.isNull("isDeleted")))
				.list();
		for(Station station:stations) {
			StationVo vo=new StationVo();
			vo.setId(station.getId());
			vo.setStation(station.getStationName());
			vo.setContactPerson(station.getContactPerson());
			vo.setAddress(station.getAddress());
			vo.setContactNumber(station.getContactNumber());
			vos.add(vo);
		}
		return vos;
	}
	
	public void deletestation(Long id) {
		Station station=this.getStationById(id);
		station.setIsDeleted(true);
		this.sessionFactory.getCurrentSession().save(station);
		
	}
	
	
	public Station getStationById(Long id) {
		return (Station) this.sessionFactory.getCurrentSession().createCriteria(Station.class)
				.add(Restrictions.eq("id", id)).uniqueResult();
	}
	
	



}
