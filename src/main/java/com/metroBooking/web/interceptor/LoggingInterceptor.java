package com.metroBooking.web.interceptor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingInterceptor {

	@Before("execution(* com.metroBooking.web.*.*.*(..))")
	public void beforeExecution(JoinPoint jp) {
		System.out.println("Entry Method ::: " + jp.getSignature().getName() + " Class ::: "
				+ jp.getTarget().getClass().getSimpleName());
	}

	@After("execution(* com.metroBooking.web.*.*.*(..))")
	public void afterExecution(JoinPoint jp) {
		System.out.println("Exit Method ::: " + jp.getSignature().getName() + " Class ::: "
				+ jp.getTarget().getClass().getSimpleName());
	}
}
