package com.metroBooking.web.service;

import java.util.List;

import com.metroBooking.web.vo.SlotVo;

public interface SlotService {
	
	/**
	 * method to save slot info
	 * 
	 * @param vo
	 */
	public void saveslot(SlotVo vo);

	/**
	 * method to get slot info by id
	 * 
	 * @param id
	 * @return
	 */
	public SlotVo getslot(Long id);

	
	/**
	 * method to fetch all slots
	 * 
	 * @return
	 */
	public List<SlotVo> getAllslots();
	
	/**
	 * method to delete slot
	 * @param id
	 */
	public void deleteslot(Long id);

}
