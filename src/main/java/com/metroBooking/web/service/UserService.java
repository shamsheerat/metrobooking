package com.metroBooking.web.service;

import java.util.List;

import com.metroBooking.web.vo.UserVo;

public interface UserService {

	/**
	 * method to save user info
	 * 
	 * @param vo
	 */
	public void saveUser(UserVo vo);

	/**
	 * method to get user info by id
	 * 
	 * @param id
	 * @return
	 */
	public UserVo getUser(Long id);

	/**
	 * method to get user info by username
	 * 
	 * @param userName
	 * @return
	 */
	public UserVo getUser(String userName);

	/**
	 * method to update user info
	 * 
	 * @param vo
	 */
	public void updateUser(UserVo vo);

	/**
	 * method to fetch all users
	 * 
	 * @return
	 */
	public List<UserVo> getAllUsers();
	
	/**
	 * method to delete user
	 * @param id
	 */
	public void deleteUser(Long id);
}
