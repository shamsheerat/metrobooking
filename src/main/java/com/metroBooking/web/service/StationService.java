package com.metroBooking.web.service;

import java.util.List;

import com.metroBooking.web.vo.StationVo;


public interface StationService {


	/**
	 * method to save station info
	 * 
	 * @param vo
	 */
	public void savestation(StationVo vo);

	/**
	 * method to get station info by id
	 * 
	 * @param id
	 * @return
	 */
	public StationVo getstation(Long id);

	
	/**
	 * method to fetch all stations
	 * 
	 * @return
	 */
	public List<StationVo> getAllstations();
	
	/**
	 * method to delete station
	 * @param id
	 */
	public void deletestation(Long id);
}
