package com.metroBooking.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metroBooking.web.dao.UserDao;
import com.metroBooking.web.service.UserService;
import com.metroBooking.web.vo.UserVo;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public void saveUser(UserVo vo) {
		userDao.saveUser(vo);
	}

	public UserVo getUser(Long id) {
		return userDao.getUser(id);
	}

	public UserVo getUser(String userName) {
		UserVo userVo = userDao.getUser(userName);
		return userVo;
	}

	public void updateUser(UserVo vo) {
		userDao.updateUser(vo);
	}

	public List<UserVo> getAllUsers() {
		return userDao.getAllUsers();
	}

	public void deleteUser(Long id) {
		userDao.deleteUser(id);
	}

}
