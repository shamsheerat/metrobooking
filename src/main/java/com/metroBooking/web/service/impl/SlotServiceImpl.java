package com.metroBooking.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metroBooking.web.dao.SlotDao;
import com.metroBooking.web.service.SlotService;
import com.metroBooking.web.vo.SlotVo;


@Service
public class SlotServiceImpl implements SlotService{
	
	@Autowired
	private SlotDao slotDao;
	
	
	public void saveslot(SlotVo vo) {
		slotDao.saveslot(vo);
	}
	
	public SlotVo getslot(Long id) {
		return slotDao.getslot(id);
	}

	
	public List<SlotVo> getAllslots(){
		return slotDao.getAllslots();
	}
	
	
	public void deleteslot(Long id) {
		slotDao.deleteslot(id);
	}

}
