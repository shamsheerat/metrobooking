package com.metroBooking.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metroBooking.web.dao.StationDao;
import com.metroBooking.web.service.StationService;
import com.metroBooking.web.vo.StationVo;


@Service
public class StationServiceImpl implements StationService{

	@Autowired
	private StationDao stationDao;
	
	
	public void savestation(StationVo vo) {
		stationDao.savestation(vo);
	}
	
	public StationVo getstation(Long id) {
		return stationDao.getstation(id);
	}

	
	public List<StationVo> getAllstations(){
		return stationDao.getAllstations();
	}
	
	
	public void deletestation(Long id) {
		stationDao.deletestation(id);
	}
}
