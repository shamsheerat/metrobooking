package com.metroBooking.web.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.metroBooking.web.interfaces.AuditEntity;

@Entity
@Table(name="metro_station")
public class Station implements AuditEntity, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6927635210669442847L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name="station_name")
	private String stationName;
	
	@Column(name="address")
	private String address;
	
	@Column(name="contactPerson")
	private String contactPerson;
	
	@Column(name="contactNumber")
	private String contactNumber;
	
	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="created_by")
	private String createdBy;

	@Column(name="updated_on")
	private Date updatedOn;
	
	@Column(name="updated_by")
	private String updatedBy;

	@Column(name="created_on")
	private Date createdOn;
	
	@OneToMany(mappedBy="stationPriceId.stationFrom")
	private Set<StationPrice> stationFromPrices=new HashSet<StationPrice>();
	
	@OneToMany(mappedBy="stationPriceId.stationTo")
	private Set<StationPrice> stationToPrices=new HashSet<StationPrice>();

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public Set<StationPrice> getStationFromPrices() {
		return stationFromPrices;
	}

	public void setStationFromPrices(Set<StationPrice> stationFromPrices) {
		this.stationFromPrices = stationFromPrices;
	}

	public Set<StationPrice> getStationToPrices() {
		return stationToPrices;
	}

	public void setStationToPrices(Set<StationPrice> stationToPrices) {
		this.stationToPrices = stationToPrices;
	}
	
	

}
