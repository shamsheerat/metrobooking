package com.metroBooking.web.entities;

import java.beans.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "station_price_chart")
@AssociationOverrides({
		@AssociationOverride(name = "stationPriceId.stationFrom", joinColumns = @JoinColumn(name = "from_station_id")),
		@AssociationOverride(name = "stationPriceId.amount", joinColumns = @JoinColumn(name = "amount")),
		@AssociationOverride(name = "stationPriceId.stationTo", joinColumns = @JoinColumn(name = "to_station_id")) })
public class StationPrice implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2342523186283265081L;

	@EmbeddedId
	private StationPriceId stationPriceId = new StationPriceId();

	@Transient
	public Station getStationFrom() {
		return stationPriceId.getStationFrom();
	}

	public void setStationFrom(Station stationFrom) {
		this.stationPriceId.setStationFrom(stationFrom);
	}

	@Transient
	public Station getStationTo() {
		return stationPriceId.getStationTo();
	}

	public void setStationTo(Station stationTo) {
		this.stationPriceId.setStationTo(stationTo);
	}

	@Transient
	public BigDecimal getAmount() {
		return stationPriceId.getAmount();
	}

	public void setAmount(BigDecimal amount) {
		this.stationPriceId.setAmount(amount);
	}
}
