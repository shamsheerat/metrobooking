package com.metroBooking.web.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class StationPriceId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4249443095174273833L;

	@ManyToOne
	private Station stationFrom;
	
	@ManyToOne
	private Station stationTo;

	@Column(name = "amount")
	private BigDecimal amount;

	public Station getStationFrom() {
		return stationFrom;
	}

	public void setStationFrom(Station stationFrom) {
		this.stationFrom = stationFrom;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Station getStationTo() {
		return stationTo;
	}

	public void setStationTo(Station stationTo) {
		this.stationTo = stationTo;
	}
}
